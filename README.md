# GitLab CI/CD with Apigee API Deployment

This repository showcases a sample continuous integration and deployment (CI/CD) pipeline using GitLab and Google Cloud's Apigee API Platform.

## Prerequisites

-   GitLab account and a project repository
-   Google Cloud Account with Apigee enabled
-   [Apigee CLI](https://github.com/apigee/apigeecli) installed on your local system or GitLab runner
-   Basic understanding of CI/CD concepts

## Pipeline Overview

This CI/CD pipeline is defined in the `.gitlab-ci.yml` file. It consists of two stages: `deploy` and `undeploy`.

The `deploy` stage zips the Apigee API proxy, imports it into Apigee, lists all APIs and deploys the new API. It is set to run only on the `main` branch.

The `undeploy` stage undeploys and deletes the specified API from Apigee. This stage is a manual job, meaning it must be manually triggered from the GitLab interface.

## Setup

1.  Fork or clone this repository to your GitLab account.
2.  Set up the Google Cloud SDK and Apigee CLI in your local environment or GitLab Runner.
3.  Ensure that you have access to an active Apigee environment and note down your `APIGEE_ENV` and `GCP_PROJECT_ID`.
4.  Store your Apigee service account credentials in a GitLab variable named `APIGEE_SA_CRED`. This variable should hold the base64-encoded contents of your service account's JSON key file.
5.  Update the `deploy` and `undeploy` stages in the `.gitlab-ci.yml` file with your project and environment details.

## Usage

1.  Make your changes to the Apigee API proxy files in the `apiproxy/` directory.
2.  Commit and push your changes to the `main` branch. This will automatically trigger the `deploy` job in your GitLab pipeline.
3.  To undeploy and delete an API, navigate to the pipeline in GitLab and manually trigger the `undeploy` job.
